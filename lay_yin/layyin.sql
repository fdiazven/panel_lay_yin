-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2021 at 07:36 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `layyin`
--

-- --------------------------------------------------------

--
-- Table structure for table `candidatos`
--

CREATE TABLE `candidatos` (
  `id_candidato` int(11) NOT NULL,
  `nombre_completo` varchar(100) NOT NULL,
  `cedula` varchar(8) NOT NULL,
  `estado_civil` varchar(30) DEFAULT NULL,
  `fecha_nacimiento` date NOT NULL,
  `zona` int(11) NOT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `personas_a_cargo` int(11) DEFAULT NULL,
  `telefono` varchar(11) DEFAULT NULL,
  `email_candidato` varchar(65) DEFAULT NULL,
  `estatus_candidato` varchar(50) NOT NULL DEFAULT 'Nuevo',
  `fecha_creacion` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_modificacion` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `candidatos`
--

INSERT INTO `candidatos` (`id_candidato`, `nombre_completo`, `cedula`, `estado_civil`, `fecha_nacimiento`, `zona`, `direccion`, `personas_a_cargo`, `telefono`, `email_candidato`, `estatus_candidato`, `fecha_creacion`, `fecha_modificacion`) VALUES
(1, 'Vajda Izabella', '16254632', 'Soltera', '1980-01-20', 1, '7558 TL  Hengelo', 0, '04244265879', 'VajdaIzabella@rhyta.com', 'Pendiente', '2021-03-23 11:42:23', '2021-03-23 11:47:30'),
(2, 'Vinicius Pinto Barbosa', '27451231', 'Soltero', '1983-11-18', 2, '768 33 Morkovice-Slížany ', 1, '04162546312', 'ViniciusPintoBarbosa@teleworm.us', 'Rechazado', '2021-03-23 11:43:35', '2021-03-23 11:47:34'),
(3, 'Vojtěch ﻿Němec', '17452136', 'Casado', '1981-07-12', 3, '880 Kirkjubæjarklaustur ', 2, '04244151236', 'VojtechNemec@jourrapide.com', 'Aprobado', '2021-03-23 11:44:26', '2021-03-23 11:47:39'),
(4, 'Laura J. Tate', '24154421', 'Casada', '1997-12-19', 4, '81050-Montanaro CE ', 3, '04244100017', 'LauraJTate@armyspy.com', 'Nuevo', '2021-03-23 11:45:57', '2021-03-23 11:45:57');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nombre_completo` varchar(150) NOT NULL,
  `email` varchar(255) NOT NULL,
  `usuario` varchar(150) NOT NULL,
  `password` varchar(200) NOT NULL,
  `estatus` varchar(50) NOT NULL DEFAULT 'ACTIVO',
  `fecha_creacion` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_modificacion` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nombre_completo`, `email`, `usuario`, `password`, `estatus`, `fecha_creacion`, `fecha_modificacion`) VALUES
(1, 'Administrador', 'prueba@gmail.com', 'admin', '$2y$12$p/N.2dhIUtEbYReu5bRQ7uwYV8JJII39K9w.5q2v5Nf/yZjMHPUGy', 'ACTIVO', '2021-03-23 11:40:21', '2021-04-06 15:43:05'),
(2, 'Francisco Diaz', 'fran@gmail.com', 'fdiazven', '$2y$12$0TkWXCvDvjkCl1DqP.EVn.umQSDTeDtQw9dFfa050ho0xHLxLhNmC', 'ACTIVO', '2021-04-06 15:42:13', '2021-04-06 15:42:13');

-- --------------------------------------------------------

--
-- Table structure for table `zonas`
--

CREATE TABLE `zonas` (
  `id_zona` int(11) NOT NULL,
  `zonas` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `zonas`
--

INSERT INTO `zonas` (`id_zona`, `zonas`) VALUES
(1, 'Naguanagua'),
(2, 'San Diego'),
(3, 'Valencia Norte'),
(4, 'Valencia Sur'),
(5, 'Flor Amarillo'),
(6, 'Los Guayos'),
(7, 'Guacara'),
(8, 'San Joaquin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `candidatos`
--
ALTER TABLE `candidatos`
  ADD PRIMARY KEY (`id_candidato`),
  ADD UNIQUE KEY `CEDULA` (`cedula`),
  ADD UNIQUE KEY `EMAIL` (`email_candidato`),
  ADD KEY `FK_Zona` (`zona`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `usuario` (`usuario`);

--
-- Indexes for table `zonas`
--
ALTER TABLE `zonas`
  ADD PRIMARY KEY (`id_zona`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `candidatos`
--
ALTER TABLE `candidatos`
  MODIFY `id_candidato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `zonas`
--
ALTER TABLE `zonas`
  MODIFY `id_zona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `candidatos`
--
ALTER TABLE `candidatos`
  ADD CONSTRAINT `FK_Zona` FOREIGN KEY (`zona`) REFERENCES `zonas` (`id_zona`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

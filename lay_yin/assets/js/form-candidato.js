/*
	*PRUEBAS DE FORMULARIO CANDIDATO*
	jQuery para pruebas de formulario
*/

$(document).ready(function(){

	console.log('jQuery working'); //Prueba 

	fetchZona(); //Mostrar las zonas en formulario registro 

	$('#register').on('submit', function(e){ //Captura el evento submit del formulario
		e.preventDefault(); //Evitar que se recargue la pagina
		
		let zona = $('#zona').val(); //Usado para verificar

		const postData = { //Datos a enviar al php
			n: $('#nombre').val(),
			c: $('#cedula').val(),
			e: $('#civil').val(),
			fn: $('#nacimiento').val(),
			z: $('#zona').val(),
			d: $('#direccion').val(),
			p: $('#personas').val(),
			t: $('#telefono').val(),
			em: $('#email').val()
		}

		if (zona == '0') { //Si no esta seleccionada una zona

			alert('Elige una zona por favor'); //Alerta
		
		}else{
		
			$.post('scriptPHP/candidatos/prueba_candidato.php', postData, function(response){ //Metodo post para registro
				alert(response); //Alerta de respuesta
				$('#register')[0].reset(); //Resetea formulario
			});
		
		}
	});

	function fetchZona(){ //Funcion para las zonas en registro
		$.ajax({ //Metodo ajax
			url: 'scriptPHP/candidatos/fetchzona.php', //Peticion
			type: 'GET', //Tipo
			success: function(response){ //Exitoso

				let zona = JSON.parse(response); //Agarra la respuesta y la convierte en JSON
				let template = ''; //Plantilla vacia

				zona.forEach(zona => { //forEach para rellenar plantilla

					template += ` 
						<option value="${zona.id}">${zona.zona}</option>
					`
				
				});

				$('#zona').html(template); //Rellenar el select
			}
		});
	}
});

$(document).ready(function(){

	// console.log("jQuery is working"); //Prueba

	/*
		*LOGIN*	
	*/

	//=========LOGIN==================

	$('#btn-lg').click(function(){ //Click en sign in
	
		if ($('#email').val() == '' || $('#password').val() == '') { //Si esta vacio alguno de los datos

			alert('Verifique los datos ingresados.');
		
		}else{ //Si lleno todo

			const postLg = { //Datos a enviar
				login: 1,
				email: $('#email').val(),
				pass: $('#password').val()
			}

			$.ajax({ //Metodo ajax
				url: 'scriptPHP/action.php', //Script para la respuesta
				type: 'POST', //Metodo a enviar
				data: postLg, //Datos
				success: function(response){ //Respuesta
					
					if (response.indexOf('exitosamente') >= 0 ) { //Si todo salio bien
					
						window.location = 'index.php'; //Redirige a index.php
					
					}else{

						alert(response); //Si no salen alertas 
					
					}
				}
			});
		}		
	});
});

/*
	*GESTION DE USUARIO*
*/

//==========REGISTRO============
$("#rg-btn").on("click", rgUser); //Al dar click en guardar (Registrar)

fetchUser(); //Funcion para ver listado

function rgUser(){ //Funcion para registrar

	let namerg = $('#namerg').val(); //Guarda variables (valores)
    let emailrg = $('#emailrg').val();
    let userrg = $('#userrg').val();
    let passwordrg = $('#passwordrg').val();
    let cpasswordrg = $('#cpasswordrg').val();	

    if (namerg == '' || emailrg == '' || userrg == '' || passwordrg == '' || cpasswordrg == '') { //Si esta vacio algo

        alert("Por favor, rellene todas las casillas...");

    }else if (passwordrg != cpasswordrg){ //Si las contrasenas no coinciden

        alert("Las contraseñas no coinciden");

    }else{ //Si todo esta bien 

    	$.ajax({ //Llamada a ajax
            url: 'scriptPHP/action.php', //Script para respuesta
            method: 'POST', //Metodo
            data: { //Datos a enviar
                register: 1,
                nombrePHP: namerg,
                emailPHP: emailrg,
                userPHP: userrg,
                passPHP: passwordrg
            },
            success: function(response){ //Una vez exitoso llama funcion que escuche respuesta

            	if (response.indexOf('exitosamente') >= 0) { //Si la respuesta es exitosamente

            		alert(response); //Registro exitoso
            		fetchUser(); //Actualiza listado
            		$('#registrar')[0].reset(); //Reinicia formulario

            	}else{

            		alert(response); //Error si no es exitoso 
            	
            	}
            }
        }); 
    }
}

//======== MOSTRAR LISTADO ==========
function fetchUser(){ //Funcion para mostrar listado
	$.ajax({ //Metodo ajax
		url: 'scriptPHP/usuarios/fetchusers.php', //Script para respuesta
		type: 'GET', //Metodo
		success: function(response){ //Una vez exitoso
			let users = JSON.parse(response); //Convierte el string en un json
			let template = ''; //Plantilla
			users.forEach(user => { //Bucle para rellenar la plantilla
				template += ` 
					<tr userId="${user.id}"> 
						<td>${user.name}</td>
						<td>${user.correo}</td>
						<td>${user.usuario}</td>
						<td>
                        	<button id="btn-modify" type="button" class="btn btn-default" data-toggle="modal" data-target="#mrusuario">Acciones</button>
                    	</td>                    	
                    	<td>
                    		<div id="status" class="btn btn-success">
                    			<span>${user.estatus}</span>
                    		</div>
                    	</td>
					</tr>
				`
			});
			$('#users').html(template); //Rellenar plantilla
		}
	});
}

//AL MODIFICAR MOSTRAR LOS DATOS
$(document).on('click', '#btn-modify', function(){ //Click en boton modificar
	let element = $(this)[0].parentElement.parentElement; //Agarra el tr
	let id = $(element).attr('userId'); //Del tr saca el id
	$.post('scriptPHP/usuarios/user-single.php', {id}, function(response){ //Manda a user-single el id
		const userm = JSON.parse(response); //Convierte en json la respuesta
		$('#name_modify').val(userm.name); //Rellena los inputs
		$('#email_modify').val(userm.email);
		$('#usuario_modify').val(userm.usuario);
		$('#pass_modify').val(userm.password);
		$('#cpass_modify').val(userm.password);
		$('#userModifyId').val(userm.id);
	});
});

//ACTUALIZAR DATOS 

$('#actualizar').on('click', function(){ //Al dar click en boton actualizar

	if ($('#pass_modify').val() != $('#cpass_modify').val()) { //Si las contrase;as son diferentes
		alert("Su contraseña no coincide, vuelva a intentarlo"); 
	
	}else{ //Si no 

		const postMod = { //Pasa valores
			modify: '1',
			id: $('#userModifyId').val(),
			name: $('#name_modify').val(),
			email: $('#email_modify').val(),
			usuario: $('#usuario_modify').val(),
			pass: $('#pass_modify').val()
		}

		$.post('scriptPHP/action.php', postMod, function(response){ //Metodo post
			alert(response); //Muestra respuesta
			fetchUser(); //Actualiza
		});
	}
});


//CAMBIAR ESTATUS A ELIMINADO

$('#eliminar').on('click', function(){ //Al dar click en eliminar

	if (confirm('Suspender a este usuario?')) { //Confirma si quieres eliminar usuario

		const postDelete = { //Datos a enviar
			delete: '1',
			id: $('#userModifyId').val()
		}

		$.post('scriptPHP/action.php', postDelete, function(response){ //Metodo post
			alert(response); //Da la respuesta
			fetchUser(); //Actualiza usuarios 
		});
	}
	
});
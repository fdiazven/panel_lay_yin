//LLAMADO DE GESTION DE USUARIO
$('#usuario').click(function(){
    $.ajax({
        url: "section/gestion_user.html",
        beforeSend : function(){
            $('#main-content').text("Cargando...");
        },
        success : function(data){
            $('#main-content').html(data);
        }
    });
});

/*
    *CANDIDATOS*
*/

fetchCandidatos(); //Funcion actualizar candidatos

//Funcion para actualizar lista de candidatos
function fetchCandidatos(){ 
	$.ajax({ //Metodo Ajax
		url: 'scriptPHP/candidatos/fetchcandidatos.php', //Peticion
		type: 'GET', //Metodo
		success: function(response){ //Exitoso

			let candidatos = JSON.parse(response); //Convierte respuesta string a Json
			let template = ''; //Plantilla
			candidatos.forEach(candidato => { //Rellenar plantilla

				template += `  
					<tr candidatoId="${candidato.id}">
                    	<td>${candidato.nombre}</td>
                        <td>${candidato.email}</td>
                        <td>${candidato.telefono}</td>
                        <td>${candidato.edad}</td>
                        <td>${candidato.zona}</td>
                        <td>
                            <button id="nuevocandidato" type="button" class="btn btn-info" data-toggle="modal" data-target="#formModal">${candidato.estatus}</button>
                        </td>
                    </tr>
				`
			
            });
			
            $('#candidatos').html(template); //Insertar plantilla en index
		}
	});
//BOTONES
// <button id="nuevocandidato" type="button" class="btn btn-info" data-toggle="modal" data-target="#formModal">Nuevo</button>
// <button type="button" class="btn btn-success">Aprobado</button>
// <button type="button" class="btn btn-danger">Rechazado</button>
// <button type="button" class="btn btn-warning">Pendiente</button>
}


//========Listar los datos del candidato presionado==========
$(document).on('click', '#nuevocandidato', function(){ //Funcion al dar click
    
    let element = $(this)[0].parentElement.parentElement; //Agarra donde se encuentra el id
    let id = $(element).attr('candidatoid'); //Coge el id
    $.post('scriptPHP/candidatos/candidato-single.php', {id}, function(response){ //Funcion post
    
        const candidato = JSON.parse(response); //Convierte en json la respuesta
        $('#candidatom').val(candidato.id); //Rellena los inputs
        $('#namecandidato').val(candidato.nombre);
        $('#cedulacandidato').val(candidato.cedula);
        $('#civilcandidato').val(candidato.civil);
        $('#brithdaycandidato').val(candidato.nacimiento);
        $('#edadcandidato').val(candidato.edad);
        $('#zonacandidato').val(candidato.zona);
        $('#direccioncandidato').val(candidato.direccion);
        $('#pcargocandidato').val(candidato.cargo);
        $('#telefonocandidato').val(candidato.telefono);
        $('#correocandidato').val(candidato.email);
    
    });
});

//=============CAMBIAR STATUS===========
//Aprobado
$('#aprobado').on('click', function(){ 
    
    if (confirm('Aprobar a este usuario?')) { //Confirmar

        const postAprobado = { //Datos a enviar
            aprobado: '1',
            id: $('#candidatom').val()
        }

        $.post('scriptPHP/candidatos/change_status.php', postAprobado, function(response){ //Metodo post
            alert(response); //Alerta de responder
            fetchCandidatos(); //Actualizar candidatos
        })
    }
});

//Rechazado
$('#rechazado').on('click', function(){
    
    if (confirm('Rechazar a este usuario?')) { //Confirmar

        const postRechazar = { //Datos a enviar
            rechazar: '1',
            id: $('#candidatom').val()
        }

        $.post('scriptPHP/candidatos/change_status.php', postRechazar, function(response){ //Metodo post
            alert(response); //Alerta de responder
            fetchCandidatos(); //Actualizar candidatos
        })
    }
});

//Pendiente
$('#pendiente').on('click', function(){

    const postPendiente = { //Datos a enviar
        pendiente: '1',
        id: $('#candidatom').val()
    }

    $.post('scriptPHP/candidatos/change_status.php', postPendiente, function(response){ //Metodo post
        alert(response); //Alerta de responder
        fetchCandidatos(); //Actualizar candidatos
    })
});
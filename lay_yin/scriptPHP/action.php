<?php 

	/*
		*ACCIONES*
	*/

	require 'conexion.php'; //Conexion BBDD
	include 'functions.php'; //Funciones 

	/*
		*LOGIN*
		ACCION PARA INGRESAR A INDEX
	*/

	if (isset($_POST['login'])) { //Si alguien intenta iniciar sesion
		
		$email = $_POST['email']; //Guarda variables
		$pass = $_POST['pass'];

		$result = login($email, $pass); //Funcion de login 

		echo $result['msg']; //Imprime resultado
		
	}

	/*
		*GESTION DE USUARIO*
		ACCION PARA REGISTRAR USUARIOS AL SISTEMA
	*/

	if (isset($_POST['register'])) { //Registro
		
		$nombre = $_POST['nombrePHP']; //Guardar datos en variables
		$email = $_POST['emailPHP'];
		$user =  $_POST['userPHP'];
		$pass =  $_POST['passPHP'];

		$result = registerUser($nombre, $email, $user, $pass); //Funcion de registro

		echo $result['msg']; //Imprime resultado
		
	}

	/*
		*GESTION DE USUARIO*
		ACCION PARA MODIFICAR UN USUARIO
	*/

	if (isset($_POST['modify'])) { //Si esta seteado modify
		
		$id = $_POST['id']; //Agarra inputs
		$nombre = $_POST['name'];
		$email = $_POST['email'];
		$user = $_POST['usuario'];
		$pass = $_POST['pass'];

		$result = modifyUser($id, $nombre, $email, $user, $pass); //Funcion de modificar

		echo $result['msg']; //Imprime resultado
			
	}

	/*
		*GESTION DE USUARIO*
		CAMBIAR ESTATUS A ELIMINADO
	*/

	if (isset($_POST['delete'])) { //Si esta seteado delete
		
		$id = $_POST['id']; //Agarra el id

		$result = deleteUser($id); //Funcion de delete

		echo $result['msg']; // Imprime resultado

	}


 ?>
<?php 

	require 'conexion.php'; //Requiere conexion 

	$_SESSION['conn'] = $conn; //Variable de conexion 

	function login($email, $pass){ //Funcion para ingresar
		
		$response = ''; //Respuesta vacia
		
		$query = "SELECT * 
					FROM users 
					WHERE email = '$email'"; //Consulta

		$row = mysqli_query($_SESSION['conn'], $query); //Ejecuta consulta

		if (mysqli_num_rows($row) > 0) { //Si email coincide

			$result = mysqli_fetch_assoc($row); //Haz una array
			
			if ($result['estatus'] == 'ACTIVO'){ //Si esta activo el email

				if (password_verify($pass, $result['password'])){ //Si verifica contraseña
					
					session_start(); //Ingresa sesion 
					$_SESSION['nombre'] = $result['nombre_completo']; //Nombre de bienvenida
					$_SESSION['loggedIN'] = '1'; //Saber que inicio correctamente
					$_SESSION['email'] = $result['email']; //Email

					$response = array(
						'code' => 200,
					    'msg' => 'Ingresado exitosamente'
					); //Respuesta

				}else{
				
					$response = array(
						'code' => 400,
					    'msg' => 'Contraseña incorrecta'
					); //Respuesta

				}

			}else{

				$response = array(
					'code' => 400,
					'msg' => 'Usuario Suspendido'
				); //Respuesta
				
			}
			
		}else{

			$response = array(
				'code' => 500,
			    'msg' => 'Email Incorrecto'
			); //Respuesta
			
		}

		return $response; //Retorna la repuesta

	}

	function registerUser($nombre, $email, $user, $pass){ //Funcion para registrar usuario

		$response = ''; //Respuesta vacia

		$conf = array( //Coste de hash
			'cost' => 12
		);

		$passh = password_hash($pass, PASSWORD_BCRYPT, $conf); //Haz pass hash

		$verifyemail = mysqli_query($_SESSION['conn'], "SELECT * 
														FROM users 
														WHERE email = '$email'"); //Verificar

		$verifyuser = mysqli_query($_SESSION['conn'], "SELECT * 
														FROM users 
														WHERE usuario = '$user'");

		if (mysqli_num_rows($verifyemail) > 0) {
			
			$response = array(
				'code' => 400,
				'msg' => 'Ya el email esta en uso' //Respuesta
			);

		}elseif (mysqli_num_rows($verifyuser) > 0) {
			
			$response = array(
				'code' => 400,
				'msg' => 'Ya el usuario esta en uso' //Respuesta
			);

		}else{

			mysqli_query($_SESSION['conn'], "INSERT INTO users (nombre_completo, email, usuario, password) 
											 VALUES ('$nombre', '$email', '$user', '$passh')"); //Consulta para registrar
			
			$response = array(
				'code' => 200,
				'msg' => 'Usuario registrado exitosamente' //Respuesta
			);

		}

		return $response; //Retorna la repuesta
 
	}

	function modifyUser($id, $nombre, $email, $user, $pass){ //Funcion para modificar usuario

		$response = ''; //Respuesta vacia

		$conf = array( //Coste de hash
			'cost' => 12
		);

		if (!empty($pass)) { //Si password no esta vacio
			
			$passh = password_hash($pass, PASSWORD_BCRYPT, $conf); //Hazla hash

			//Consulta
			$query = "UPDATE users 
					  SET nombre_completo = '$nombre', email = '$email', usuario = '$user', password = '$passh' 
					  WHERE id = '$id'"; 
		
			$result = mysqli_query($_SESSION['conn'], $query); //Resultado

			if (!$result) {
				
				$response = array(
					'code' => 400,
					'msg' => 'Query Failed' //Respuesta
				);

			}else{

				$response = array(
					'code' => 200,
					'msg' => 'Usuario actualizado exitosamente' //Respuesta
				);

			}
		
		}else{

			$response = array(
				'code' => 500,
				'msg' => 'Error. La contraseña no puede estar vacia' //Respuesta
			);

		}
	
		return $response; //Retorna respuesta

	}

	function deleteUser($id){ //Funcion para suspender usuario

		$response = ''; //Respuesta vacia

		$query = "UPDATE users 
				  SET estatus = 'ELIMINADO' 
				  WHERE id = '$id'"; //Consulta

		$result = mysqli_query($_SESSION['conn'], $query); //Resultado

		if (!$result) {
			
			$response = array( //Respuesta
				'code' => 400,
				'msg' => 'Query Failed'
			);

		}else{

			$response = array( //Respuesta
				'code' => 200,
				'msg' => 'El usuario se ha suspendido correctamente'
			);

		}

		return $response; //Retornar respuesta
	}

	function statusAprobado($id){ //Funcion para aprobar candidato

		$response = ''; //Respuesta vacia

		$query = "UPDATE candidatos 
				  SET estatus_candidato = 'Aprobado' 
				  WHERE id_candidato = '$id'"; //Actualiza

		$result = mysqli_query($_SESSION['conn'], $query); //Ejecuta

		if (!$result) { //Si no hay resultado
		
			$response = array(
				'code' => 400,
				'msg' => 'Query Failed'
			); //Error Query
		
		}else{ //Si hay resultado

			$response = array(
				'code' => 200,
				'msg' => 'El usuario ha sido aprobado'
			); //Usuario Aprobado

		}

		return $response; //Retornar respuesta

	}

	function statusRechazado($id){ //Funcion para rechazar candidato

		$response = ''; //Respuesta vacia

		$query = "UPDATE candidatos 
				  SET estatus_candidato = 'Rechazado' 
				  WHERE id_candidato = '$id'"; //Actualiza

		$result = mysqli_query($_SESSION['conn'], $query); //Ejecuta

		if (!$result) { //Si no hay resultado
		
			$response = array(
				'code' => 400,
				'msg' => 'Query Failed'
			); //Error Query
		
		}else{ //Si hay resultado 

			$response = array(
				'code' => 200,
				'msg' => 'El usuario ha sido rechazado'
			); //usuario rechazado

		}

		return $response; //Retornar respuesta

	}

	function statusPendiente($id){ //Funcion para pendiente candidato

		$response = ''; //Respuesta vacia

		$query = "UPDATE candidatos 
				  SET estatus_candidato = 'Pendiente' 
				  WHERE id_candidato = '$id'"; //Actualiza

		$result = mysqli_query($_SESSION['conn'], $query); //Ejecuta

		if (!$result) { //Si no hay resultado
		
			$response = array(
				'code' => 400,
				'msg' => 'Query Failed'
			);//Error query
		
		}else{ //Si hay resultado 

			$response = array(
				'code' => 200,
				'msg' => 'El usuario esta en lista de pendiente'
			); //usuario pendiente

		}

		return $response; //Retornar respuesta

	}

	function candidatoSingle($id){ //Funcion para mostrar informacion de usuario

		$response = ''; //Respuesta

		$query = "SELECT *, YEAR(CURDATE())-YEAR(candidatos.fecha_nacimiento) AS Edad
				FROM candidatos
				INNER JOIN zonas ON candidatos.zona = zonas.id_zona
				WHERE id_candidato = '$id'"; //Consulta

		$result = mysqli_query($_SESSION['conn'], $query); //Ejecutar resultado

		if (!$result) { //Si no hay resultado
			
			$response = array(
				'code' => 400, 
				'msg' => 'Query Failed'
			);

		}else{

			$json = array(); //Haz json

			while ($row = mysqli_fetch_array($result)) { //Guardar datos de la consulta

				$json[] = array( //Guarda los resultados 
					'id' => $row['id_candidato'],
					'nombre' => $row['nombre_completo'],
					'cedula' => $row['cedula'],
					'civil' => $row['estado_civil'],
					'nacimiento' => $row['fecha_nacimiento'],
					'edad' => $row['Edad'],
					'zona' => $row['zonas'],
					'direccion' => $row['direccion'],
					'cargo' => $row['personas_a_cargo'],
					'telefono' => $row['telefono'],
					'email' => $row['email_candidato'],
				);

			}

			$response = json_encode($json[0]); //Guarda jsonstring en respuesta
		
		}

		return $response; //Retorna respuesta

	}

	function fetchCandidatos(){ //Funcion para mostrar candidatos

		$response = ''; //Respuesta vacia

		$query = "SELECT candidatos.id_candidato, candidatos.nombre_completo, candidatos.email_candidato, candidatos.telefono, YEAR(CURDATE())-YEAR(candidatos.fecha_nacimiento) AS Edad, zonas.zonas, candidatos.estatus_candidato
			FROM candidatos
			INNER JOIN zonas ON candidatos.zona = zonas.id_zona
			ORDER BY id_candidato DESC";  //CONSULTA

		$result = mysqli_query($_SESSION['conn'], $query); //Resultado

		if (!$result) { //Si no hay resultado
			
			$response = array(
				'code' => 400, 
				'msg' => 'Query Failed'
			);

		}else{

			$json = array(); //Json

			while ($row = mysqli_fetch_array($result)) { //Recorre elementos
			
				$json[] = array( //Mete a array
					'id' => $row['id_candidato'],
					'nombre' => $row['nombre_completo'],
					'email' => $row['email_candidato'],
					'telefono' => $row['telefono'],
					'edad' => $row['Edad'],
					'zona' => $row['zonas'],
					'estatus' => $row['estatus_candidato']
				);
			
			}

			$response = json_encode($json); //Respuesta

		}

		return $response; //Retorna respuesta

	}

	function  fetchUsers(){ //Funcion para mostrar usuarios

		$response = ''; //Respuesta vacia

		$query = "SELECT * 
				FROM users 
				ORDER BY id DESC"; //Consulta 

		$result = mysqli_query($_SESSION['conn'], $query); //Ejecutar

		if (!$result) { //Si no hay resultado

			$response = array(
				'code' => 400, 
				'msg' => 'Query Failed'
			);

		}else{ //Si no

			$json = array(); //Variable json y hazla un array

			while ($row = mysqli_fetch_array($result)){ //Guarda los resultados en row con un array

				$json[] = array( //Ingresalos en el array json
					'name' => $row['nombre_completo'],
					'correo' => $row['email'],
					'usuario' => $row['usuario'],
					'id' => $row['id'],
					'estatus' => $row['estatus']
				);
			}	

			$response = json_encode($json); //Respuesta

		}

		return $response; //Retornar respuesta

	}

	function userSingle($id){ //Mostrar informacion del usuario seleccionado

		$response = ''; //Respuesta vacia

		$query = "SELECT * 
				 FROM users 
				 WHERE id = '$id'"; //Agarra los datos con el mismo id

		$result = mysqli_query($_SESSION['conn'], $query); //Ejecutar la consulta

		if (!$result) { //Si no hubo resultado exitoso

			$response = array(
				'code' => 400, 
				'msg' => 'Query Failed'
			);
	
		}else{

			$json = array(); //Objeto Json
	
			while ($row = mysqli_fetch_array($result)) { //Recorre los datos y los guarda

				$json[] = array( //Ingresa los datos al json
					'id' => $row['id'],
					'name' => $row['nombre_completo'],
					'email' => $row['email'],
					'usuario' => $row['usuario'],
					'password' => ''
				);
			}

			$response = json_encode($json[0]); //Respuesta

		}

		return $response; //Retornar respuesta

	}

	function fetchZona(){ //Mostrar zonas para formulario

		$response = ''; //Respuesta vacia

		$query = "SELECT * FROM zonas"; //Consulta

		$result = mysqli_query($_SESSION['conn'], $query); //Resultado

		if (!$result) { //Si no hay resultado

			$response = array(
				'code' => 400, 
				'msg' => 'Query Failed'
			);
		
		}else{

			$json = array(); //Objeto Json

			$json[0] = array( //El primer elemento sera Seleccione con valor 0
				'id' => '0',
				'zona' => 'Seleccione'
			);

			while ($row = mysqli_fetch_array($result)) { //Recorre los datos y los guarda
					
				$json[] = array( //Mete en el json los datos
					'id' => $row['id_zona'],
					'zona' => $row['zonas']
				);
			
			}

			$response = json_encode($json); //Respuesta
		}

		return $response; //Retorna respuesta
	}

	function registroCandidato($nombre, $cedula, $estado_civil, $nacimiento, $zona, $direccion, $personas, $telefono, $email){//Registro candidato

		$response = ''; //Respuesta vacia

		//Consulta
		$query = "INSERT INTO candidatos(nombre_completo, cedula, estado_civil, fecha_nacimiento, zona, direccion, personas_a_cargo, telefono, email_candidato) 
		VALUES(('$nombre'), ('$cedula'), ('$estado_civil'), ('$nacimiento'), ('$zona'), ('$direccion'), ('$personas'), ('$telefono'), ('$email'))";

		$result = mysqli_query($_SESSION['conn'], $query); //Resultado

		if (!$result) { //Si no hay resultado
			
			$response = array(
				'code' => 400, 
				'msg' => 'Query Failed'
			);

		}else{ //Si hay

			$response = array(
				'code' => 200,
				'msg' => 'Usuario registrado exitosamente' //Respuesta
			);

		}

		return $response; //Retornar respuesta
	}

 ?>
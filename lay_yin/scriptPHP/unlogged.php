<?php 

	/*
		*LOGIN*
		SI INTENTA ENTRAR AL INDEX 
		SIN HABER INICIADO SESION
		TE REDIRIGE A EL LOGIN
	*/

	session_start(); //Iniciamos sesion

	if (!isset($_SESSION['loggedIN'])) { //Si no inicio sesion
		
		header('location: pages-login.php'); //Localiza a formulario
		exit();
	
	}


 ?>
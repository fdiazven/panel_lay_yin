<?php 

	/*
		CAMBIOS DE STATUS DE LOS CANDIDATOS
	*/

	include '../conexion.php'; //Incluye la conexion
	include '../functions.php'; //Funciones 

	if (isset($_POST['aprobado'])) { //Si se aprobo
		
		$id = $_POST['id']; //Agarra el id

		$result = statusAprobado($id); //Funcion para aprobar

		echo $result['msg']; //Imprime respuesta
		
	}

	if (isset($_POST['rechazar'])) { //Si se rechazo
		
		$id = $_POST['id']; //Agarra el id

		$result = statusRechazado($id); //Funcion para rechazar

		echo $result['msg']; //Imprime respuesta
		
	}

	if (isset($_POST['pendiente'])) { //Si se coloco en pendiente
		
		$id = $_POST['id']; //Agarra el id

		$result = statusPendiente($id); //Funcion para pendiente

		echo $result['msg']; //Imprime respuesta
	}
 
?>
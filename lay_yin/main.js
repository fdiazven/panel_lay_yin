const electron = require('electron');
const{app, BrowserWindow} = electron;
const args = process.argv;
const path = require('path');
const url = require('url');
let mainWindow;
app.on('ready', ()=>{
    mainWindow = new BrowserWindow({
        width: 800,
        height: 500,
        icon: 'favicon.ico'
    });
    mainWindow.setMenu(null);
    mainWindow.setTitle('Prueba');
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname,'index.php'),
        protocol: 'file',
        slashes: true
    }));
    mainWindow.on('closed', () =>{
        mainWindow = null;
    });
});